package si.uni_lj.fri.pbd.miniapp2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import java.io.IOException;
import java.lang.ref.WeakReference;

public class MainActivity extends AppCompatActivity {
    // TAG for logging and debugging
    private static final String TAG = MainActivity.class.getSimpleName();

    // connected service and boolean to tell if it is bound
    private MediaPlayerService mediaPlayerService;
    private boolean serviceBound;

    // TextViews (so we can change their text in onClickListeners)
    private TextView songTitle;
    private TextView duration;

    // constants for MusicUpdateReceiver
    public static final String ACTION_UPDATE_TITLE = "Update title";
    public static final String ACTION_UPDATE_DURATION = "Update duration";
    public static final String ACTION_RESET = "Reset UI";
    public static final String ACTION_EXIT = "Exit application";

    // Music update receiver
    private MusicUpdateReceiver musicUpdateReceiver;

    // connection with the service
    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.d(TAG, "MediaPlayerService bound");

            // get mediaPlayerService from binder
            MediaPlayerService.MediaServiceBinder binder = (MediaPlayerService.MediaServiceBinder) service;
            mediaPlayerService = binder.getService();
            serviceBound = true;

            // update UI
            songTitle.setText(mediaPlayerService.songTitle);
            duration.setText(mediaPlayerService.timeToString());
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.d(TAG, "MediaPlayerService disconnected");

            serviceBound = false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "Activity created");

        setContentView(R.layout.activity_main);

        // we get TextViews
        songTitle = findViewById(R.id.song_title);
        duration = findViewById(R.id.duration);
    }

    @Override
    protected void onStart() {
        super.onStart();

        Log.d(TAG, "Activity started");

        // we start the service
        Intent i = new Intent(this, MediaPlayerService.class);
        i.setAction(MediaPlayerService.ACTION_START);
        startService(i);

        // we bind the service
        bindService(i, serviceConnection, 0);

        // register the broadcast receiver
        if (musicUpdateReceiver == null) musicUpdateReceiver = new MusicUpdateReceiver(this);
        IntentFilter filter = new IntentFilter(ACTION_UPDATE_TITLE);
        filter.addAction(ACTION_UPDATE_DURATION);
        filter.addAction(ACTION_RESET);
        filter.addAction(ACTION_EXIT);
        registerReceiver(musicUpdateReceiver, filter);

        // set onClickListeners for buttons so clicks call corresponding mediaPlayerServiceMethod
        ImageButton playButton = findViewById(R.id.play_button);
        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (serviceBound) {
                    try {
                        // we set the title of the music
                        mediaPlayerService.play();
                    } catch (IOException e) {}
                }
                else {
                    Log.d(TAG, "MediaPlayerService is not bound");
                }
            }
        });

        ImageButton pauseButton = findViewById(R.id.pause_button);
        pauseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (serviceBound) {
                    mediaPlayerService.pause();
                }
                else {
                    Log.d(TAG, "MediaPlayerService is not bound");
                }
            }
        });

        ImageButton stopButton = findViewById(R.id.stop_button);
        stopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (serviceBound) {
                    mediaPlayerService.stop();
                }
                else {
                    Log.d(TAG, "MediaPlayerService is not bound");
                }
            }
        });

        Button exitButton = findViewById(R.id.exit_button);
        exitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // we stop the service
                Intent stopIntent = new Intent(MainActivity.this, MediaPlayerService.class);
                stopService(stopIntent);

                exitApplication();
            }
        });

        Button gestureOn = findViewById(R.id.gesture_on_button);
        gestureOn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (serviceBound) {
                    mediaPlayerService.gesturesOn();
                }
                else {
                    Log.d(TAG, "MediaPlayerService is not bound");
                }
            }
        });

        Button gestureOff = findViewById(R.id.gesture_off_button);
        gestureOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (serviceBound) {
                    mediaPlayerService.gesturesOff();
                }
                else {
                    Log.d(TAG, "MediaPlayerService is not bound");
                }
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();

        // unbind the service
        if (serviceBound) {
            // unregister the broadcast receiver
            unregisterReceiver(musicUpdateReceiver);

            unbindService(serviceConnection);
            serviceBound = false;
        }

        else {
            stopService(new Intent(this, MediaPlayerService.class));
        }
    }

    // method that unbinds the service and exits the app
    private void exitApplication() {
        // we unbind the service
        if (serviceBound) {
            unbindService(serviceConnection);
            serviceBound = false;
            unregisterReceiver(musicUpdateReceiver);
        }

        finish();

        // we exit the app
        System.exit(0);
    }

    // Receiver that receives info about updating the UI
    private class MusicUpdateReceiver extends BroadcastReceiver {
        private final WeakReference<MainActivity> activity;

        public MusicUpdateReceiver(MainActivity activity) {
            this.activity = new WeakReference<>(activity);
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "Broadcast received");

            // we update the title
            if (intent.getAction().equals(ACTION_UPDATE_TITLE)) {
                activity.get().songTitle.setText(mediaPlayerService.songTitle);
            }

            // we update the duration
            if (intent.getAction().equals(ACTION_UPDATE_DURATION)) {
                activity.get().duration.setText(mediaPlayerService.timeToString());
            }

            // we reset the UI
            if (intent.getAction().equals(ACTION_RESET)) {
                activity.get().songTitle.setText(R.string.song_default);
                activity.get().duration.setText(R.string.duration_default);
            }

            // we exit the application
            if (intent.getAction().equals(ACTION_EXIT)) {
                activity.get().exitApplication();
            }
        }
    }
}
