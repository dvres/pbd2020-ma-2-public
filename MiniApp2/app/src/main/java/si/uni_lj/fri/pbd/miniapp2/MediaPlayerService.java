package si.uni_lj.fri.pbd.miniapp2;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.res.AssetFileDescriptor;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Random;

public class MediaPlayerService extends Service {
    // TAG of the Service for logging
    private static final String TAG = MediaPlayerService.class.getSimpleName();

    // booleans to determine the state of the Service
    private boolean isMusicPlaying;
    private boolean isPaused;
    private boolean isStopped;

    // variables that help measure duration of the song (for older APIs)
    private long startTime, elapsedTime;

    // Actions that can be set by MainActivity, notification or AccelerationService
    public static final String ACTION_START = "Start service";
    public static final String ACTION_PLAY = "Play music";
    public static final String ACTION_PAUSE = "Pause music";
    private static final String ACTION_STOP = "Stop music";
    private static final String ACTION_EXIT = "Stop service";

    // local binder
    private final IBinder serviceBinder = new MediaServiceBinder();

    // MediaPlayer that will play the songs
    private MediaPlayer mediaPlayer = new MediaPlayer();

    // title of the song that is currently playing
    public String songTitle;

    // icon of the song that is currently playing
    public int icon;

    // object of class Random for selecting the next song
    private Random r;

    // IDs required for notification
    private final static String NOTIFICATION_CHANNEL_ID = "Mini app 2 music player";
    private final static int NOTIFICATION_ID = 42;

    // Notification manager and builder required to build and update the notification
    private NotificationManager nm;
    private NotificationCompat.Builder builder;

    // ArrayList of actions for notification (so we can change play and pause)
    private ArrayList<NotificationCompat.Action> actions;

    // constant ID for handler
    private static final int MSG_UPDATE_DURATION = 1;

    // handler for periodical UI updates (song duration updates)
    private UIUpdateHandler uiUpdateHandler;

    // boolean that tells whether AccelerationService is active
    private boolean accelerationBound;

    // bound AccelerationService
    private AccelerationService accelerationService;

    // receiver for AccelerationServiceBroadcast
    private AccelerationUpdateReceiver accelerationUpdateReceiver;

    // connection to AccelerationService
    private ServiceConnection accelerationConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.d(TAG, "Acceleration service bound");

            AccelerationService.AccelerationServiceBinder binder = (AccelerationService.AccelerationServiceBinder) service;
            accelerationService = binder.getService();
            accelerationBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.d(TAG, "Acceleration service disconnected");

            accelerationBound = false;
        }
    };

    @Override
    public void onCreate() {
        //super.onCreate();
        Log.d(TAG, "Service created");

        // initialize the booleans
        isMusicPlaying = false;
        isPaused = false;
        isStopped = true;

        // no song is playing yet
        songTitle = getString(R.string.song_default);
        icon = R.drawable.logo;

        // initialize the update handler
        uiUpdateHandler = new UIUpdateHandler(this);

        // initialize the random seed
        r = new Random();

        // we set onCompletionListener for MediaPlayer (so when the song is finished it starts new one)
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                try {
                    // we play another music
                    isMusicPlaying = false;
                    isStopped = true;
                    play();

                    // update song title on UI (duration is already being updated since song finished)
                    updateTitle();
                } catch (IOException e) {}
            }
        });

        // initialize the notificationManager and createNotificationChannel
        nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        createNotificationChannel();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // service is started from MainActivity
        if (intent.getAction().equals(ACTION_START)) {
            Log.d(TAG, "Service started");

            // start notification
            startForeground(NOTIFICATION_ID, createNotification());
        }

        // play action called
        else if (intent.getAction().equals(ACTION_PLAY)) {
            try {
                play();
            } catch (IOException e) {}
        }

        // pause action called
        else if (intent.getAction().equals(ACTION_PAUSE)) pause();

        // stop action called
        else if (intent.getAction().equals(ACTION_STOP)) stop();

        // exit action called
        else if (intent.getAction().equals(ACTION_EXIT)) {
            if (isMusicPlaying) stopDurationUpdate();
            // we remove the notification and stop the mediaPlayer
            stopForeground(true);
            if (!isStopped) mediaPlayer.stop();

            // we tell the MainActivity to stop
            Intent i = new Intent();
            i.setAction(MainActivity.ACTION_EXIT);
            sendBroadcast(i);

            // stop the AccelerationService
            if (accelerationBound) {
                unbindService(accelerationConnection);
                accelerationBound = false;

                stopService(new Intent(this, AccelerationService.class));

                unregisterReceiver(accelerationUpdateReceiver);
            }

            // stop the service
            stopSelf();
        }

        return Service.START_STICKY;
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "Service stopped");

        // stop updating the UI
        if (isMusicPlaying) stopDurationUpdate();
        stopForeground(true);
        if(!isStopped) mediaPlayer.stop();

        // stop the AccelerationService
        if (accelerationBound) {
            unbindService(accelerationConnection);
            accelerationBound = false;

            stopService(new Intent(this, AccelerationService.class));

            unregisterReceiver(accelerationUpdateReceiver);
        }
    }

    // Method that starts playing music
    public void play() throws IOException {
        // we check the current state of the service
        if (isPaused) {
            // we resume the current song
            mediaPlayer.start();
            // we get the time when the music resumed
            startTime = System.currentTimeMillis();

            // start duration update
            startDurationUpdate();

            // we change the state of the service
            isPaused = false;
            isMusicPlaying = true;

            // change the action button
            actions.get(0).title = "Pause";
            actions.get(0).actionIntent = pauseIntent();
            nm.notify(NOTIFICATION_ID, builder.build());
        }
        else if (isStopped) {
            // all music files
            String[] files = new String[] {
                    "mp3/UEFA Champions League - Main Theme.mp3",
                    "mp3/Game of Thrones - The Rains Of Castamereby The National.mp3",
                    "mp3/Harry_Potter_Theme_Song_Hedwigs_Theme.mp3",
                    "mp3/Indiana Jones.mp3",
                    "mp3/Star Wars Theme Song By John Williams.mp3"
            };

            int[] icons = new int[] {
                    R.drawable.champions_league,
                    R.drawable.house_lannister,
                    R.drawable.harry_potter,
                    R.drawable.indiana_jones,
                    R.drawable.star_wars
            };

            // we randomly select the index
            int index = r.nextInt(files.length);
            icon = icons[index];

            String file = files[index];
            // we set the song title (we remove the mp3/ prefix and .mp3 suffix
            songTitle = file.substring(4, file.length()-4);

            // we set new resource to play mp3 and start it
            try {
                AssetFileDescriptor afd = getAssets().openFd(file);
                mediaPlayer.reset();
                mediaPlayer.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
                mediaPlayer.prepare();
                mediaPlayer.start();

                // we get the time when music started and set elapsed time to 0
                startTime = System.currentTimeMillis();
                elapsedTime = 0;

                // update UI
                updateTitle();
                startDurationUpdate();

                // set the icon
                builder.setLargeIcon(BitmapFactory.decodeResource(getResources(), icons[index]));
                nm.notify(NOTIFICATION_ID, builder.build());

                // we change the state of the service
                isStopped = false;
                isMusicPlaying = true;
            }
            catch (IOException e) {
                Log.d(TAG, "File \""+files[index]+"\" not found in assets");
            }

            // change the action button
            actions.get(0).title = "Pause";
            actions.get(0).actionIntent = pauseIntent();
            nm.notify(NOTIFICATION_ID, builder.build());
        }
    }

    // Method that pauses the current music
    public void pause() {
        // we check the state of the service
        if (isPaused || isStopped) return;
        if (isMusicPlaying) {
            mediaPlayer.pause();
            // we get the current time and increase the elapsed time
            long endTime = System.currentTimeMillis();
            elapsedTime = elapsedTime + (endTime - startTime) / 1000;

            // we change the state of the service
            isMusicPlaying = false;
            isPaused = true;

            // stop updating the UI
            stopDurationUpdate();

            // change the action button
            actions.get(0).title = "Play";
            actions.get(0).actionIntent = playIntent();
            nm.notify(NOTIFICATION_ID, createNotification());

            // last update of main activity
            Intent i = new Intent();
            i.setAction(MainActivity.ACTION_UPDATE_DURATION);
            sendBroadcast(i);
        }
    }

    // Method that stops the current music
    public void stop() {
        // we check the state of the service
        if (isStopped) return;

        // we stop the mediaPlayer
        mediaPlayer.stop();

        // we reset the elapsed time
        elapsedTime = 0;

        // Stop UI update
        stopDurationUpdate();
        // update the MainActivity
        Intent i = new Intent();
        i.setAction(MainActivity.ACTION_RESET);
        sendBroadcast(i);

        // set song title to default
        songTitle = getString(R.string.song_default);

        // we change the state of the service
        if (isMusicPlaying) isMusicPlaying = false;
        else if (isPaused) isPaused = false;
        isStopped = true;

        // icon is default
        icon = R.drawable.logo;

        // update the notification
        builder.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.logo));
        builder.setContentTitle(getString(R.string.song_default));
        builder.setContentText(getString(R.string.duration_default));
        actions.get(0).title = "Play";
        actions.get(0).actionIntent = playIntent();
        nm.notify(NOTIFICATION_ID, builder.build());
    }

    public void gesturesOn() {
        // if accelerationService is already bound we let user know with Toast
        if (accelerationBound) {
            Toast.makeText(this, "Gestures are already on", Toast.LENGTH_LONG).show();
            return;
        }

        // start the service
        Intent i = new Intent(this, AccelerationService.class);
        startService(i);

        // bind the service
        bindService(i, accelerationConnection, 0);

        // register the receiver
        if (accelerationUpdateReceiver == null) accelerationUpdateReceiver = new AccelerationUpdateReceiver(this);
        IntentFilter filter = new IntentFilter(ACTION_PLAY);
        filter.addAction(ACTION_PAUSE);
        registerReceiver(accelerationUpdateReceiver, filter);

        // let user know the gestures are turned on
        Toast.makeText(this, "Gestures turned on", Toast.LENGTH_LONG).show();
    }

    public void gesturesOff() {
        // if accelerationService is already disconnected we let user know with Toast
        if (!accelerationBound) {
            Toast.makeText(this, "Gestures are already off", Toast.LENGTH_LONG).show();
            return;
        }

        // we stop the service
        unbindService(accelerationConnection);
        accelerationBound = false;
        stopService(new Intent(this, AccelerationService.class));

        // unregister the receiver
        unregisterReceiver(accelerationUpdateReceiver);

        // let user know gestures are turned off
        Toast.makeText(this, "Gestures turned off", Toast.LENGTH_LONG).show();
    }

    // method that returns the length of the song that is currently playing
    public int songLength() {
        // default duration if no music is playing
        if (isStopped) return 0;

        // divide with 1000 because time is in milliseconds
        return mediaPlayer.getDuration() / 1000;
    }

    // method that returns the duration of current song
    public long songDuration() {
        // default duration if no music is playing
        if (isStopped) return 0;

        // getTimestamp requires API atl least 23
        if (Build.VERSION.SDK_INT >= 23) {
            // divide with 1000000 because time is in microseconds
            return mediaPlayer.getTimestamp().getAnchorMediaTimeUs() / 1000000;
        }

        // if is paused we only return the time when it was last paused
        if (isPaused) {
            return elapsedTime;
        }

        // else we add current running time to the time when it was last paused
        long time = System.currentTimeMillis();
        return elapsedTime + (time - startTime) / 1000;
    }

    // method that returns given time in String format H:MM:SS/H:MM:SS
    private String timeToString(long time) {
        int hours = (int) (time / 3600);
        time = time - 3600*hours;
        int minutes = (int) (time / 60);
        int seconds = (int) (time - 60*minutes);

        // we put time in format H:MM:SS
        return String.format("%d:%02d:%02d", hours, minutes, seconds);
    }

    // method that returns current song time in String format H:MM:SS/H:MM:SS
    public String timeToString() {
        return timeToString(songDuration()) + "/" + timeToString(songLength());
    }

    // method that sends broadcast to MainActivity to update song title and updates notification
    private void updateTitle() {
        // update the MainActivity
        Intent i = new Intent();
        i.setAction(MainActivity.ACTION_UPDATE_TITLE);
        sendBroadcast(i);

        // update the notification
        builder.setContentTitle(songTitle);
        nm.notify(NOTIFICATION_ID, builder.build());
    }

    // method that starts handler which updates UI every second
    private void startDurationUpdate() {
        uiUpdateHandler.sendEmptyMessage(MSG_UPDATE_DURATION);
    }

    // method that stops the handler
    private void stopDurationUpdate() {
        uiUpdateHandler.removeMessages(MSG_UPDATE_DURATION);
    }

    // method for notification channel
    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT < 26) return;
        else {
            NotificationChannel nc = new NotificationChannel(NOTIFICATION_CHANNEL_ID, getString(R.string.notification_name), NotificationManager.IMPORTANCE_LOW);
            nc.setDescription(getString(R.string.notification_description));
            nc.enableLights(true);
            nc.setLightColor(Color.RED);
            nc.enableVibration(true);

            //NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            nm.createNotificationChannel(nc);
        }
    }

    // method for notification
    private Notification createNotification() {
        // intents for stop and exit actions
        Intent stopIntent = new Intent(this, MediaPlayerService.class);
        stopIntent.setAction(ACTION_STOP);
        PendingIntent stopPendingIntent = PendingIntent.getService(this, 0, stopIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent exitIntent = new Intent(this, MediaPlayerService.class);
        exitIntent.setAction(ACTION_EXIT);
        PendingIntent exitPendingIntent = PendingIntent.getService(this, 0, exitIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        // we add all actions in arrayList so we have direct access to them
        actions = new ArrayList<>();
        // we set the first action according to the current state of the service
        if (isMusicPlaying) {
            actions.add(new NotificationCompat.Action(android.R.drawable.ic_media_pause, "Pause", pauseIntent()));
        }
        else {
            actions.add(new NotificationCompat.Action(android.R.drawable.ic_media_play, "Play", playIntent()));
        }
        actions.add(new NotificationCompat.Action(R.drawable.ic_stop_black_36dp, "Stop", stopPendingIntent));
        actions.add(new NotificationCompat.Action(android.R.drawable.ic_lock_power_off, "Exit", exitPendingIntent));

        // we build the notification
        builder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_notification)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), icon))
                .setColorized(true)
                .setColor(getResources().getColor(R.color.colorPrimary))
                .setContentTitle(songTitle)
                .setContentText(timeToString())
                .setChannelId(NOTIFICATION_CHANNEL_ID)
                .addAction(actions.get(0))
                .addAction(actions.get(1))
                .addAction(actions.get(2));

        // intent that starts MainActivity if we click on the notification
        Intent resultIntent = new Intent(this, MainActivity.class);
        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(this, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(resultPendingIntent);

        return builder.build();
    }

    // method that builds PendingIntent for play action
    private PendingIntent playIntent() {
        Intent playIntent = new Intent(this, MediaPlayerService.class);
        playIntent.setAction(ACTION_PLAY);
        return PendingIntent.getService(this, 0, playIntent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    // method that builds PendingIntent for pause action
    private PendingIntent pauseIntent() {
        Intent pauseIntent = new Intent(this, MediaPlayerService.class);
        pauseIntent.setAction(ACTION_PAUSE);
        return PendingIntent.getService(this, 0, pauseIntent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "Binding service");

        return serviceBinder;
    }

    // local binder for communication with MainActivity
    public class MediaServiceBinder extends Binder {
        MediaPlayerService getService() {return MediaPlayerService.this;}
    }

    // Handler that updates song duration on UI
    static class UIUpdateHandler extends Handler {
        // we update the UI every second since this is the smallest unit in our timer
        private final static int UPDATE_TIME_MS = 1000;
        private final WeakReference<MediaPlayerService> service;

        public UIUpdateHandler(MediaPlayerService service) {
            this.service = new WeakReference<>(service);
        }

        @Override
        public void handleMessage(@NonNull Message msg) {
            if (msg.what == MSG_UPDATE_DURATION) {
                Log.d(TAG, "Updating the duration of the song");
                MediaPlayerService mps = service.get();

                // send broadcast to MainActivity
                Intent i = new Intent();
                i.setAction(MainActivity.ACTION_UPDATE_DURATION);
                mps.sendBroadcast(i);

                // update notification
                mps.builder.setContentText(mps.timeToString());
                mps.nm.notify(mps.NOTIFICATION_ID, mps.builder.build());

                sendEmptyMessageDelayed(MSG_UPDATE_DURATION, UPDATE_TIME_MS);
            }
        }
    }

    // Broadcast receiver for updates from acceleration service
    private class AccelerationUpdateReceiver extends BroadcastReceiver {
        private final WeakReference<MediaPlayerService> service;

        public AccelerationUpdateReceiver(MediaPlayerService service) {
            this.service = new WeakReference<>(service);
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "Broadcast received");

            // we check which action was send and call the right method
            if (intent.getAction().equals(ACTION_PLAY)) {
                try {play();} catch (IOException e) {}
            }

            if (intent.getAction().equals(ACTION_PAUSE)) pause();
        }
    }
}
