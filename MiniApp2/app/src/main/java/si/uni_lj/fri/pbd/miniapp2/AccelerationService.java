package si.uni_lj.fri.pbd.miniapp2;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;

public class AccelerationService extends Service implements SensorEventListener {
    // TAG for logging, debugging
    private static final String TAG = "Acceleration service";

    // binder
    private IBinder serviceBinder = new AccelerationServiceBinder();

    // sensorManager and sensor
    private SensorManager sensorManager;
    private Sensor accelerometer;

    // last sensor readings
    private double x;
    private double y;
    private double z;

    // boolean that tells if the sensor is triggered for the first time
    private boolean firstTime;
    // the time when we updated the mediaPlayerService for the last time
    private long lastUpdate;

    @Override
    public void onCreate() {
        Log.d(TAG, "Service created");

        // initialize firstTime
        firstTime = true;
        // initialize lastUpdate
        lastUpdate = System.currentTimeMillis();

        // initialize sensorManager
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

        // register sensor listener
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        if (accelerometer != null) {
            sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        }
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "Service stopped");

        // unregister sensor listener
        sensorManager.unregisterListener(this);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "Binding service");

        return serviceBinder;
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        Log.d(TAG, "Reading the sensor");

        // time of sample
        long time = System.currentTimeMillis();

        // if this is the first trigger we only sample coordinates
        if (firstTime) {
            Log.d(TAG, "First sample");

            x = event.values[0];
            y = event.values[1];
            z = event.values[2];

            firstTime = false;
            return;
        }

        // check if we have to sample
        if (time - lastUpdate < 500) {
            return;
        }

        Log.d(TAG, "Update time");

        // this is the most recent update
        lastUpdate = time;

        // read the sensor values
        double xt = event.values[0];
        double yt = event.values[1];
        double zt = event.values[2];

        // absolute changes for each coordinate
        double dx = Math.abs(xt - x);
        double dy = Math.abs(yt - y);
        double dz = Math.abs(zt - z);

        // set the coordinates
        x = xt;
        y = yt;
        z = zt;

        // ignore the noise
        if (dx < 5) dx = 0;
        if (dz < 5) dz = 0;

        // check for HORIZONTAL
        if (dx > dz) {
            // publish pause command
            Intent i = new Intent();
            i.setAction(MediaPlayerService.ACTION_PAUSE);
            sendBroadcast(i);
        }

        // check for VERTICAL
        else if (dz > dx) {
            // publish play command
            Intent i = new Intent();
            i.setAction(MediaPlayerService.ACTION_PLAY);
            sendBroadcast(i);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    // local binder
    public class AccelerationServiceBinder extends Binder {
        AccelerationService getService() {return AccelerationService.this;}
    }
}
